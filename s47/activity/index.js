// alert("Hello Lulu")

//querySelector used to select a specific object/element from our document
console.log(document.querySelector("#txt-first-name"));

//document refers to the whole page
console.log(document);


/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/
console.log(document.getElementById("txt-first-name"));


//======== EVENT and EVENT LISTENERS ============

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name")

// console.log(txtFirstName);
// console.log(txtLastName);
// console.log(spanFullName);


// /*

// 	Event
// 		ex: clock, hover, keypress and many other events

// 	Event Listeners
// 		Allows us to let our user/s intereact with our page. With eaach clck or hover there is an event which triggers a function/task.

// 	Syntax:
// 		selectedElement.addEventListener("event", function);

// */

// //"keyup", "keypress"
// // txtFirstName.addEventListener("keyup", (event) => {

// // 	//innerHTML property retrieves the HTML content within the element
// // 	//"value" property retrieves the value from the HTML element
// // 	spanFullName.innerHTML = txtFirstName.value
// // });





// txtFirstName.addEventListener("keyup", (event) => {
// 	console.log(event);
// 	console.log(event.target);
// 	console.log(event.target.value);
// });

// /*
// 	The "event" argument contains the information on the triggered event.
// 	The "event.target" contains the element where the event happened.
// 	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
// */
// // Mini activity
// const labelFirstName = document.querySelector("#label-first-name")

// labelFirstName.addEventListener("click", (event) => {
// 	alert("You clicked the first name label");
// });

///==== ACTIVITY =====

txtFirstName.addEventListener("keyup", printFullName);
txtLastName.addEventListener("keyup", printFullName);

function printFullName (event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}